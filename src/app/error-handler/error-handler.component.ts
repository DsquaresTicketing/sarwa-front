import { ErrorHandler, Injector, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandling implements ErrorHandler {
  handleError(error: any): void {
    console.log(error.message);
  }
}
