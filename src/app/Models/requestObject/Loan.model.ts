import { Base } from './base.model';

export class Loan extends Base {
    loanAmount: number;
    tenor: number;
    totalValue: number;
    downPayment: number;
    itemLoanAmount: number;
}
