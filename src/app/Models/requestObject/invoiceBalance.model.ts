import { Base } from './base.model';

export class InvoiceBalance extends Base {
  constructor(public loanAmount: number) {
    super();
  }
  Tenor: number;
  Category: number;
}
