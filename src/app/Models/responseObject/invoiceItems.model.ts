export class InvoiceItems {
  itemId: number;
  name: string;
  category: string;
  subCategory: string;
  brand: string;
  price: number;
  downPayment: number;
  installmentSeqNo: number;
  tenor: number;
  invoiceNo: string;
  monthlyPay: number;
}
