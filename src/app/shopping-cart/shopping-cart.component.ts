import { Component, OnInit, ViewChild } from '@angular/core';
import { ShoppingCartService } from '../services/shoppingCart.service';
import { LoanItem } from '../Models/requestObject/LoanItem.model';
import { CheckOutServices } from '../services/checkOut.service';
import { debug } from 'util';
import { debounce } from 'rxjs/internal/operators/debounce';
import { PackageService } from 'src/app/services/Package.service';
import { ProductServices } from 'src/app/services/Products.services';
import { ValidateBalanceService } from 'src/app/services/validateBalance.service';
import { CalculateInstallmentsService } from 'src/app/services/calcInstallments.service';
import { CommonService } from 'src/app/services/common.service';
import { ItemBalance } from 'src/app/Models/requestObject/itemBalance.model';
import { ProductRes } from 'src/app/Models/responseObject/product.response.model';
import { NgForm } from '@angular/forms';
import { InvoiceBalance } from 'src/app/Models/requestObject/invoiceBalance.model';
import { ModalDirective } from 'angular-bootstrap-md';
import { Invoice } from '../Models/requestObject/Invoice.model';
import { delay } from 'q';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  cartItems: LoanItem[];
  totalPayment: number = 0;
  totalDownPayment: number = 0
  Edittemsquantity: number;
  public Edittems = new LoanItem();
  Duration = [6, 12, 18, 24,36];
  errorMessage: string;
  invoice = new Invoice();
  originalPrice: number;
  originalDB: number;
  editIndex: number;
  loading: boolean = false;
  
  @ViewChild(ModalDirective, { static: false }) modal: ModalDirective;
  ngOnInit() {
    // debugger
    if (localStorage.myCart !== undefined) {
      this.cartItems = JSON.parse(localStorage.myCart);
      console.log(this.cartItems.map(x => x.price))
      this.totalPayment = this.cartItems.map(x => Number(x.price)).reduce((a, b) => a + b)
      this.totalDownPayment = this.cartItems.map(x => Number(x.downPayment)).reduce((a, b) => a + b)
    }
    else
      this.cartItems = []

    this.shoppingCartService.emitter.subscribe(x => {
      if (x) {
        this.cartItems = [];
        this.shoppingCartService.myCart = [];
        localStorage.removeItem("myCart");
        this.totalDownPayment = 0;
        this.totalPayment = 0
      }
    }
    )
    this.shoppingCartService.Addemitter.subscribe(x => {
      if (x != null) {
        // debugger
        this.totalPayment += Number(x.price);
        this.totalDownPayment += Number(x.downPayment)
        this.cartItems.push(x);

      }
    })

   
  }
  constructor(private packageService: PackageService, private productServices: ProductServices,
    private calcService: CalculateInstallmentsService, private validateService: ValidateBalanceService,
    private shoppingCartService: ShoppingCartService, private commonService: CommonService,
    private checkoutService: CheckOutServices) {
  }

  removeFromCartForEdit(prodCode, editItem) {
    
    this.cartItems[prodCode] = editItem
    localStorage.myCart = JSON.stringify(this.cartItems);
    this.totalPayment += Number(editItem.price);
    this.totalDownPayment += Number(editItem.downPayment)
    this.shoppingCartService.itemsQuntity = this.shoppingCartService.getItemsQuntity();
    this.shoppingCartService.totalsForInvoice = this.shoppingCartService.getTotals();
    //this.totalPayment -= this.cartItems.filter(x => x.itemId == prodCode).map(x => x.price)[0];
    //  this.totalDownPayment -= this.cartItems.filter(x => x.itemId == prodCode).map(x => x.downPayment)[0]

  }
  removeFromCart(prodIndex) {
    // var index = this.cartItems.findIndex(x => x.itemId == prodCode);
    debugger
    this.shoppingCartService.removeFromCart(prodIndex);
    this.totalPayment -= Number(this.cartItems[prodIndex].price);
    this.totalDownPayment -= Number(this.cartItems[prodIndex].downPayment);
    this.cartItems.splice(prodIndex, 1);
  }

  editFromCart(prodCode) {
    debugger
    //this.Edittemsquantity = this.cartItems.filter(x => x.itemId == prodCode).length
    this.originalPrice = Number(this.cartItems[prodCode].price)
    this.originalDB = Number(this.cartItems[prodCode].downPayment)
    
    console.log('edititems', this.cartItems[prodCode]);
     this.Edittems  = Object.assign({}, this.cartItems[prodCode])
    console.log('editAfter', this.Edittems);

    this.editIndex = prodCode;

  }

  validateBalance(formValue, form: NgForm) {
    debugger
    console.log('validate' ,this.Edittems);

    this.loading = true;
    this.commonService.showLoading = true;
    const itemBalance = new ItemBalance((formValue.totalPrice) - formValue.downPayment, formValue.tenor);
    const item = new LoanItem();
    item.tenor = formValue.tenor;
    item.downPayment = Number(formValue.downPayment);
    item.itemId = this.Edittems.itemId;
    item.nameAr = this.Edittems.nameAr;
    item.SubName = this.Edittems.SubName
    item.code = this.Edittems.code;
    item.price = Number(formValue.totalPrice)
    this.invoice.price = Number(item.price);   
    this.invoice.downPayment =  Number(item.downPayment);
    const invoiceBalance = new InvoiceBalance(this.invoice.price - this.invoice.downPayment);
    invoiceBalance.Tenor = item.tenor;
    invoiceBalance.Category = this.Edittems.categoryId;
    this.validateService.checkBalance(invoiceBalance).subscribe((res: any) => {
      if (!res.validTenor) {
        this.loading = false;
        this.errorMessage = ' الحد الاقصى لشهور التقسيط هو' + res.maxTenor;
      }
      if (res.validBalance) {
        debugger;
        this.totalPayment -= this.originalPrice;
        this.totalDownPayment -= this.originalDB;
        this.calculateInstallmentPerProduct(itemBalance, item, 1, form);

      } else {
        this.errorMessage = 'الحد الإئتماني المتاح غير كافي.';
      }
      this.commonService.showLoading = false;
    }, error => {
      this.errorMessage = '	يتعذر الإتصال بالنظام، يرجى إعادة المحاولة، في حالة إستمرار المشكلة يرجى الإتصال بمدير العلاقات التجارية المسئول.';
      this.commonService.showLoading = false;
    });
  }

  calculateInstallmentPerProduct(itemBalance: ItemBalance, item: LoanItem, quantity, form: NgForm) {
    this.commonService.showLoading = true;
    this.calcService.calculateInstallments(itemBalance).subscribe((monthlyInst: any) => {
      // this.installmentValue = +monthlyInst.installmentValue;
      item.installment = monthlyInst.installmentValue;
      item.SubName = this.Edittems.SubName;
      this.removeFromCartForEdit(this.editIndex, item);

      //  this.shoppingCartService.addToCart(item, quantity);
      this.modal.hide();
      this.commonService.showLoading = false;
      this.loading = false
      form.reset();
    });
  }

  onEvent(event) {
    event.stopPropagation()
  }

  resetCart() {
    localStorage.removeItem("myCart");
    this.cartItems = [];

  }
  submitInvoice() {
  }





}









