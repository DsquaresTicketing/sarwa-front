import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { LoginServices } from './services/Login.services';
import { PackageService } from './services/Package.service';
import { ValidateBalanceService } from './services/validateBalance.service';
import { CalculateInstallmentsService } from './services/calcInstallments.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CustomerComponent } from './customer/customer.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { SuccessComponent } from './success/success.component';
import { ProductComponent } from './product/product.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InvoiceHistoryComponent } from './invoice-history/invoice-history.component';
import { RefundPopupComponent } from './refund-popup/refund-popup.component';
import { ProductServices } from './services/Products.services';
import { ShoppingCartService } from './services/shoppingCart.service';
import { ConfirmationAddToCartComponent } from './confirmation-add-to-cart/confirmation-add-to-cart.component';
import { CheckOutServices } from './services/checkOut.service';
import { ErrorHandling } from './error-handler/error-handler.component';
import { MDBBootstrapModule, TooltipModule } from 'angular-bootstrap-md';
import { WavesModule, ButtonsModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NumberDirective } from './directives/numberOnly';
import { PrintingService } from './services/printing.service';
import { ReceiptComponent } from './receipt/receipt.component';
import { ReplacePipe } from './directives/replace.pipe';
import { RefundReceiptComponent } from './refund-receipt/refund-receipt.component';
import { BnNgIdleService } from 'bn-ng-idle';
import { BotDetectCaptchaModule } from 'angular-captcha';
import { CaptchaServices } from './services/captcha.Service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CustomerComponent,
    ShoppingCartComponent,
    CheckOutComponent,
    SuccessComponent,
    ProductComponent,
    NavbarComponent,
    InvoiceHistoryComponent,
    RefundPopupComponent,
    ConfirmationAddToCartComponent,
    NumberDirective,
    ReceiptComponent,
    ReplacePipe,
    RefundReceiptComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    TooltipModule.forRoot(),
    ButtonsModule,
    WavesModule.forRoot(),
    BrowserAnimationsModule,
    BotDetectCaptchaModule
  ],
  providers: [
    LoginServices,
    PackageService,
    ProductServices,
    ValidateBalanceService,
    CheckOutServices,
    CalculateInstallmentsService,
    ShoppingCartService,
    PrintingService,
    BnNgIdleService,
    CaptchaServices,
    { provide: ErrorHandler, useClass: ErrorHandling },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
