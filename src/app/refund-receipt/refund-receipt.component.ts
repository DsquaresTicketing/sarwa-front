import { Component, OnInit } from '@angular/core';
import { PrintingService } from '../services/printing.service';

@Component({
  selector: 'app-refund-receipt',
  templateUrl: './refund-receipt.component.html',
  styleUrls: ['./refund-receipt.component.scss']
})
export class RefundReceiptComponent implements OnInit {
  clientId: string = localStorage.clientId;
  constructor(public printingService: PrintingService) { }

  ngOnInit() {
  }

}
