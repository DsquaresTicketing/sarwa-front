import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseDataService } from './base-data.service';
import { ItemBalance } from '../Models/requestObject/itemBalance.model';


@Injectable()
export class CalculateInstallmentsService extends BaseDataService {
    constructor(private http: HttpClient) {
      super();
    }
   calculateInstallments(itemBalnce: ItemBalance) {
       return this.http.post(this.APIEndpoint + 'CalcInstallment?sessionId=' + this.getSessionId()
       , itemBalnce, this.getAuthHeader());

    }


}
