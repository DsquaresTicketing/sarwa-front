import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { BaseDataService } from './base-data.service';
import { InvoiceBalance } from '../Models/requestObject/invoiceBalance.model';


@Injectable()
export class ValidateBalanceService extends BaseDataService {
    constructor(private http: HttpClient) {
      super();
    }
   checkBalance(invoiceBalance: InvoiceBalance) {
       return this.http.post(this.APIEndpoint + 'ValidateBalance?sessionId=' + this.getSessionId(),
        invoiceBalance, this.getAuthHeader() );
   }
}
