import { Injectable, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginCredentials } from '../Models/requestObject/Login.Models';
import { BaseDataService } from './base-data.service';


@Injectable({
  providedIn: 'root'
})
export class LoginServices extends BaseDataService {

  private LoginCred: string;
  constructor(private http: HttpClient) {
    super();
  }
  Login(loginObj: LoginCredentials) {
    this.LoginCred = loginObj.authUsername;
    return this.http.post(this.APIEndpoint + 'Account?authUsername', loginObj);

  }


  getUsername() {
    if (this.LoginCred !== undefined) {
      console.log(this.LoginCred);
      return this.LoginCred;
    }
  }
  Logout() {
    return this.http.post(this.APIEndpoint + 'Account/Logout?sessionId=' + this.getSessionId(), {}, this.getAuthHeader());
  }


}
