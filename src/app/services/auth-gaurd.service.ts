import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginServices } from './Login.services';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurd implements CanActivate {

  constructor(private router: Router, private loginService: LoginServices, private common: CommonService) { }
  canActivate() {
    if (localStorage.sessionId !== undefined || localStorage.currentUser !== undefined) {
      this.common.showNav = true;
      return true;
    } else {
      this.common.showNav = false;
      this.router.navigate(['/login']);
      return false;
    }
  }

}
