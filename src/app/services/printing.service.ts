import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { InvoiceSummaryService } from './invoice-summary.service';
import { InvoiceDetails } from '../Models/responseObject/invoiceDetails.model';
import { CommonService } from './common.service';
import { RefundItem } from '../Models/requestObject/refund-item';
import { Refund } from '../Models/responseObject/refund.model';

@Injectable({
  providedIn: 'root'
})
export class PrintingService {

  isPrinting = false;
  public invoiceDetails: InvoiceDetails = new InvoiceDetails();
  public refundDeatils: Refund = new Refund();
  constructor(private router: Router,
              private invoiceService: InvoiceSummaryService,
              private commonService: CommonService
  ) { }

  printDocument(invoiceNumber: number) {
    this.commonService.showLoading = true;
    this.invoiceService.getInvoiceDetails(localStorage.clientId, invoiceNumber).subscribe((res: any) => {
      this.invoiceDetails = res.invoiceDetails;
      console.log(this.invoiceDetails);
      this.isPrinting = true;
      this.commonService.showLoading = false;

      this.router.navigateByUrl('/print');
      this.onDataReady(invoiceNumber);
    }, err => {
      this.commonService.showLoading = false;

    });
  }
  printRefundDocument(refundNumber: number) {
    this.commonService.showLoading = true;
    this.invoiceService.getRefundeDetails(localStorage.clientId, refundNumber).subscribe((res: any) => {
      this.refundDeatils = res.refundDetails;
      console.log(this.refundDeatils);
      this.isPrinting = true;
      this.commonService.showLoading = false;

      this.router.navigateByUrl('/printrefund');
      this.onDataReady(refundNumber);
    }, err => {
      this.commonService.showLoading = false;

    });
  }
  onDataReady(invoiceNumber) {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      // this.router.navigateByUrl(`check-out/success/${invoiceNumber}`);
    });
  }
}
