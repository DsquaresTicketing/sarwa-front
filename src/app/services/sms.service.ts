import { Injectable } from '@angular/core';
import { BaseDataService } from './base-data.service';
import { HttpClient } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class SmsService extends BaseDataService {
  constructor(private http: HttpClient) {
    super();
  }
  sendOtp(clientID: string, type: number, amount: number) {
    return this.http.get(
      this.APIEndpoint +
      'sms/sendMessage?clientID=' +
      clientID +
      '&sessionId=' + this.getSessionId() +
      '&type=' + type +
      '&amount=' + amount,
      this.getAuthHeader()
    );
  }
  verifyOtp(mobileNumber, otp) {
    return this.http.get(
      this.APIEndpoint +
      'sms/VerifyOtp?mobileNo=' +
      mobileNumber +
      '&sessionId=' +
      this.getSessionId() +
      '&otp=' + otp,
      this.getAuthHeader()
    );
  }
}
