import { Component, OnInit } from '@angular/core';
import { InvoiceHistory } from '../Models/responseObject/invoiceHistory.model';
import { InvoiceSummaryService } from '../services/invoice-summary.service';
import { InvoiceDetails } from '../Models/responseObject/invoiceDetails.model';
import { CommonService } from '../services/common.service';
import { RefundItem } from '../Models/requestObject/refund-item';
import { InstallmentSeq } from '../Models/requestObject/installmentSeq';
import { Router } from '@angular/router';
import { CheckOutServices } from '../services/checkOut.service';

@Component({
  selector: 'app-invoice-history',
  templateUrl: './invoice-history.component.html',
  styleUrls: ['./invoice-history.component.scss']
})
export class InvoiceHistoryComponent implements OnInit {
  invoiceHistory: InvoiceHistory[];
  invoiceDetails: InvoiceDetails = new InvoiceDetails();
  errorMessage: string;
  invoiceHistoryerrorMessage: string;
  refundedItems = new RefundItem();

  constructor(
    private invoiceSummaryService: InvoiceSummaryService,
    private commonService: CommonService,
    private router: Router,
    private checkInOutService: CheckOutServices
  ) { }

  ngOnInit() {
  }

  getInvoiceHistory(clientId: string) {
    this.invoiceHistoryerrorMessage = '';
    localStorage.clientId = clientId;
    this.commonService.showLoading = true;
    this.invoiceSummaryService.getInvoiceHistory(clientId).subscribe(
      (res: any) => {
        this.invoiceHistory = res.invoiceSummaries;
        this.commonService.showLoading = false;
        if (this.invoiceHistory.length === 0) {
          this.invoiceHistoryerrorMessage = 'عفواً لا يوجد عمليات يمكن إسترجاعها لهذا الرقم. يرجى التأكد من صحة بيانات العميل، في حالة إستمرار المشكلة يرجى توجيه العميل للإتصال بـ16177';
        }
      },
      err => {
        this.errorMessage = 'حدث خطاء بالنظام';
        this.commonService.showLoading = false;
      }
    );
  }
  getInvoiceDetails(clientId: string, invoiceNo: number, invoice) {

    this.commonService.showLoading = true;
    this.refundedItems.clientId = clientId;
    this.invoiceSummaryService.getInvoiceDetails(clientId, invoiceNo).subscribe(
      (res: any) => {
        this.invoiceDetails = res.invoiceDetails;
        this.commonService.showLoading = false;
        this.invoiceHistory.forEach(c => (c.showDetails = false));
        invoice.showDetails = true;
        this.commonService.showLoading = false;
      },
      err => {
        this.errorMessage = "	يتعذر الإتصال بالنظام، يرجى إعادة المحاولة، في حالة إستمرار المشكلة يرجى الإتصال بمدير العلاقات التجارية المسئول.";
        this.commonService.showLoading = false;
      }
    );
  }
  addRefundedItems(
    installmentSeq: string,
    invoiceNo: string,
    index: number,
    isChecked: boolean
  ) {
    this.refundedItems.invoiceNo = invoiceNo;
    if (isChecked) {
      const item = new InstallmentSeq();
      item.installmentSeqNo = installmentSeq;
      this.refundedItems.items.push(item);
    } else {
      this.refundedItems.items.splice(index, 1);
    }
  }
  getMobileNumber() {

  }
  goToRefundView() {
    this.commonService.refundedListItem = this.refundedItems;
    localStorage.refundedItems = JSON.stringify(this.refundedItems);
    this.commonService.showLoading = true;
    this.checkInOutService.getMobileNumber(localStorage.clientId).subscribe((res: any) => {
      localStorage.mobileNo = res.mobile;
      this.router.navigateByUrl('/retrival/refund-popup');
      this.commonService.showLoading = false;
    });
  }
}
