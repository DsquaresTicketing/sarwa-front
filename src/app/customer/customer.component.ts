import { Component, ViewChild, OnInit } from '@angular/core';
import { PackageService } from '../services/Package.service';
import { ProductServices } from '../services/Products.services';
import { ValidateBalanceService } from '../services/validateBalance.service';
import { CalculateInstallmentsService } from '../services/calcInstallments.service';
import { ShoppingCartService } from '../services/shoppingCart.service';
import { PackageRes } from '../Models/responseObject/package.response.models';
import { CategoryRes } from '../Models/responseObject/category.response.model';
import { SubCategoryRes } from '../Models/responseObject/subCategory.response.model';
import { ProductRes } from '../Models/responseObject/product.response.model';
import { InvoiceBalance } from '../Models/requestObject/invoiceBalance.model';
import { ItemBalance } from '../Models/requestObject/itemBalance.model';
import { Invoice } from '../Models/requestObject/Invoice.model';
import { LoanItem } from '../Models/requestObject/LoanItem.model';
import { ModalDirective } from 'angular-bootstrap-md';
import { CommonService } from '../services/common.service';
import { NgForm } from '@angular/forms';
import { debounce } from 'rxjs/internal/operators/debounce';


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  isEnabled = true;
  isloanCal = false;
  defaultvalueForDownPayment: number;
  defaultvalueForquantity: number;
  errorMessage: string;
  packageRes = new PackageRes();
  categoryRes: CategoryRes[];
  subCategories: SubCategoryRes[];
  productList: ProductRes[];
  installmentValue = 0;
  addForm: NgForm;
  public selectedLevel = 12;
  Duration = [6, 12, 18, 24,36];
  subName: string;
  categoryId: number;
  invoice = new Invoice();
  adderrorMessage: string;
  loading = false;
  ngOnInit() {
    this.defaultvalueForDownPayment = 1;
    this.errorMessage = "";
  }

  @ViewChild(ModalDirective, { static: false }) modal: ModalDirective;

  constructor(private packageService: PackageService, private productServices: ProductServices,
    private calcService: CalculateInstallmentsService, private validateService: ValidateBalanceService,
    private shoppingCartService: ShoppingCartService, private commonService: CommonService) {
    this.defaultvalueForDownPayment = 0;
    this.defaultvalueForquantity = 1;
    this.selectedLevel = 12;

  }

  getPackage(formObj) {
    this.errorMessage = '';
    this.isEnabled = true;
    this.adderrorMessage = '';
    if (localStorage.clientId !== formObj.clientId) {
      this.shoppingCartService.fireDeleteEvent();
      this.shoppingCartService.removeShoppingCart();
    }

    localStorage.setItem('clientId', formObj.clientId);
    this.commonService.showLoading = true;
    this.packageService.getPackage(formObj.clientId).subscribe((packageData: PackageRes) => {
      this.commonService.showLoading = false;
      this.packageRes = packageData;
      this.isEnabled = false;
      if (packageData.code !== null) {
        this.getCategories();
      } else {
        this.commonService.showLoading = false;
      }
    }, error => {
      this.commonService.showLoading = false;
      this.errorMessage = error.error.errorMessage;

    });


  }
  getCategories() {
    this.productServices.getCategories().subscribe((catListData: any) => {
      this.categoryRes = catListData.Categories;
      this.commonService.showLoading = false;
    });
  }

  getSubCategories(categoryId) {
    this.commonService.showLoading = true;
    this.categoryId = categoryId;
    this.productServices.getSubCategories(categoryId).subscribe((subCatListData: any) => {
      this.subCategories = subCatListData.subCategories;
      this.productList = [];
      this.commonService.showLoading = false;
    }, error => {
      console.log(error);
      this.commonService.showLoading = false;
    });
  }

  getProducts(subCategoryId) {
    this.commonService.showLoading = true;
    this.subName = this.subCategories.filter(x => x.ID == subCategoryId).map(x => x.NameAr)[0];
    this.productServices.getProducts(subCategoryId).subscribe((productListData: any) => {
      this.productList = productListData.items;
      this.commonService.showLoading = false;
    });
  }
  resetParentForm(addForm: NgForm, cartForm) {
    this.resetcartForm(cartForm);
    this.addForm = addForm;
  }
  resetcartForm(cartForm) {
    cartForm.controls.totalPrice.setValue(0);
    cartForm.controls.downPayment.setValue(0);
    cartForm.controls.noOfMonths.setValue(12);
    cartForm.controls.quantity.setValue(1);
  }
  validateBalance(formValue, prod: ProductRes, form: NgForm) {
    debugger
    this.loading = true;
    this.commonService.showLoading = true;
    const itemBalance = new ItemBalance((formValue.totalPrice) - formValue.downPayment, formValue.noOfMonths);
    const item = new LoanItem();
    item.tenor = formValue.noOfMonths;
    item.downPayment = Number(formValue.downPayment);
    item.itemId = prod.ID;
    item.nameAr = prod.NameAr;
    item.code = prod.Code;
    prod.Name = prod.Name;
    item.SubName = this.subName;
    item.categoryId=this.categoryId;
    item.price = Number(formValue.totalPrice);
    this.invoice.price = + this.shoppingCartService.totalsForInvoice.price + (Number(item.price) * Number(formValue.quantity));
    this.invoice.downPayment = +this.shoppingCartService.totalsForInvoice.downPayment + (Number(item.downPayment) * Number(formValue.quantity));
    const invoiceBalance = new InvoiceBalance(this.invoice.price - this.invoice.downPayment);
    invoiceBalance.Tenor = item.tenor;
    invoiceBalance.Category = this.categoryId;
    this.validateService.checkBalance(invoiceBalance).subscribe((res: any) => {

      if (!res.validTenor) { 
        this.loading = false;
        this.adderrorMessage = ' الحد الاقصى لشهور التقسيط هو' + res.maxTenor;
      } else if (res.validBalance) {
        // debugger;
        this.calculateInstallmentPerProduct(itemBalance, item, formValue.quantity, form);
        this.addForm.reset();

      } else {
        this.loading = false;
        this.adderrorMessage = 'الحد الإئتماني المتاح غير كافي.';
      }
      this.commonService.showLoading = false;
    }, error => {
      this.loading = false;
      this.errorMessage = '	يتعذر الإتصال بالنظام، يرجى إعادة المحاولة، في حالة إستمرار المشكلة يرجى الإتصال بمدير العلاقات التجارية المسئول.';
      this.commonService.showLoading = false;
    });
  }

  calculateInstallmentPerProduct(itemBalance: ItemBalance, item: LoanItem, quantity, form: NgForm) {
    this.commonService.showLoading = true;
    this.calcService.calculateInstallments(itemBalance).subscribe((monthlyInst: any) => {
      // this.installmentValue = +monthlyInst.installmentValue;
      item.installment = monthlyInst.installmentValue;
      item.SubName = this.subName;
      this.addToCart(item, quantity);
      this.modal.hide();
      this.commonService.showLoading = false;
      this.loading = false;
      this.resetcartForm(form)

    });
  }
  closePopUp(form: NgForm) {
    form.reset();
    this.selectedLevel = 12;

  }
  onEvent(event) {
    event.stopPropagation()
  }
  addToCart(item: LoanItem, quantity) {

    // this.shoppingCartService.myCart=item.;

    this.shoppingCartService.addToCart(item, quantity);
  }



}
