import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { LoginComponent } from './login/login.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { SuccessComponent } from './success/success.component';
import { RefundPopupComponent } from './refund-popup/refund-popup.component';
import { AuthGaurd } from './services/auth-gaurd.service';
import { InvoiceHistoryComponent } from './invoice-history/invoice-history.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { RefundReceiptComponent } from './refund-receipt/refund-receipt.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'customer',
    pathMatch: 'full'
  },
  { path: 'customer', component: CustomerComponent, canActivate: [AuthGaurd] },
  { path: 'login', component: LoginComponent },
  {
    path: 'retrival',
    component: InvoiceHistoryComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: 'shopping-cart',
    component: ShoppingCartComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: 'check-out',
    component: CheckOutComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: 'check-out/success/:refund/:invoiceNo',
    component: SuccessComponent,
    canActivate: [AuthGaurd],
  },
  {
    path: 'retrival/refund-popup',
    component: RefundPopupComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: 'print',
    //  outlet: 'print',
    component: ReceiptComponent,
    canActivate: [AuthGaurd]
  }
  ,
  {
    path: 'printrefund',
    //  outlet: 'print',
    component: RefundReceiptComponent,
    canActivate: [AuthGaurd]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
