import { Component, OnInit } from '@angular/core';
import { InvoiceHistory } from '../Models/responseObject/invoiceHistory.model';
import { InvoiceSummaryService } from '../services/invoice-summary.service';
import { InvoiceDetails } from '../Models/responseObject/invoiceDetails.model';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-invoice-history',
  templateUrl: './invoice-history.component.html',
  styleUrls: ['./invoice-history.component.scss']
})
export class InvoiceHistoryComponent implements OnInit {
  invoiceHistory: InvoiceHistory [];
  invoiceDetails: InvoiceDetails;
  errorMessage: string;
  constructor(private invoiceSummaryService: InvoiceSummaryService,
              private commonService: CommonService) { }

  ngOnInit() {
  }

  getInvoiceHistory(clientId: string) {
    this.commonService.showLoading =true;
    this.invoiceSummaryService.getInvoiceHistory(clientId).subscribe( (res: any) => {
        this.invoiceHistory = res.InvoiceHistories;
        this.commonService.showLoading = false;
      }, err => {
        this.errorMessage = 'حدث خطأ.';
        this.commonService.showLoading = false;
      });
  }
  getInvoiceDetails(clientId: string, invoiceNo: number) {
    this.invoiceSummaryService.getInvoiceDetails(clientId, invoiceNo).subscribe( (res: any) => {
        this.invoiceDetails = res.invoiceDetails;
        this.commonService.showLoading = false;

    }, err => {
      this.errorMessage = 'حدث خطأ.';
      this.commonService.showLoading = false;

    });
  }
}
